import {Config} from "./config/config";
import {MusicBot} from "./bot/MusicBot";
import winston from "winston";
import {cli} from "./logging/formats";

// Logger setup
const logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(winston.format.timestamp(), winston.format.json(), winston.format.colorize()),
    defaultMeta: { service: 'MusicBot' },
    transports: [
        new winston.transports.File({
            filename: 'MusicBot.log',
            level: "debug",
        }),
        new winston.transports.Console({
            format: cli(),
            level: "debug",
        }),
    ],
});
const clogger = logger.child({section: "config"})
const blogger = logger.child({section: "bot"})

// Config setup
clogger.info("Loading configuration");
const config = new Config();
config.load();
config.save(); // save the config so it exists when not yet present, or updated it with new options

// test config for required parameters
if (!testConfig(config)) {
    process.exit(1);
}
clogger.info("Configuration successfully loaded");


// Instantiate our bot and launch everything
const bot = new MusicBot(config, blogger);
bot.run();


// helper functions
function testConfig(config: Config): boolean {
    const botConfig = config.bot;
    if (botConfig.isDefault('clientId') || botConfig.isDefault('clientSecret') || botConfig.isDefault('token')) {
        clogger.crit("Discord application details not configured");
        clogger.warn("Please fill in your discord application details in ./config/bot.yaml!");
        return false;
    }

    return true;
}
