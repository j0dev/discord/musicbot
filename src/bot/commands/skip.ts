import {Command} from "../libs/Command";
import {ApplicationCommandOptionData, CommandInteraction, MessageEmbed} from "discord.js";
import {EphemeralMessageState} from "../libs/enums";
import {MusicBot} from "../MusicBot";
import {CommandBuilder} from "../utils/commandBuilder";

export class Skip implements Command{
    getVersion(): number {
        return 1;
    }

    getName(): string {
        return "skip";
    }

    getDescription(): string {
        return "Skip a song";
    }

    getUsage(): string {
        return "";
    }

    getAliases(): string[] {
        return [];
    }

    getRoles(): string[] {
        return ["DJ"];
    }

    getCommandOptions(): ApplicationCommandOptionData[] {
        const options = new CommandBuilder()
            .addIntInput("amount")
            .setDescription("Amount of songs to skip (default 1)");
        return options.output();
    }

    getEphemeral(): EphemeralMessageState {
        return EphemeralMessageState.Never;
    }

    async run(bot: MusicBot, interaction: CommandInteraction): Promise<void> {
        if (!bot.channelCheck(interaction)) return;

        const player = bot.playerManager?.get(interaction.guild?.id || "0");
        if (!player) {
            return bot.sendInteractionError(interaction, "Something went wrong retrieving the player");
        }

        if (!player.playing && !player.paused) {
            return bot.sendInteractionError(interaction, "Skipping over nothing, sure thing...");
        }

        // check how many songs
        const amount = interaction.options.get("amount")?.value as number;
        if (amount > 1 && player.queue.length > 1) {
            // remove the amount minus one (for the playing track)
            player.queue.remove(0, amount - 1);
        }

        player.stop();

        let msg = "Too bad you dont like that song";
        if (amount) msg = "Too bad you dont like those songs";
        if (player.queue.length) msg += ", off to the next then..";
        const emb = new MessageEmbed()
            .setColor("PURPLE")
            .setAuthor("Skipping Song", bot.config.bot.iconNext)
            .setDescription(msg);
        return bot.sendInteraction(interaction, this.getEphemeral(), {embeds: [emb]});
    }
}
