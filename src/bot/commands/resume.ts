import {Command} from "../libs/Command";
import {ApplicationCommandOptionData, CommandInteraction, MessageEmbed} from "discord.js";
import {EphemeralMessageState} from "../libs/enums";
import {MusicBot} from "../MusicBot";

export class Resume implements Command {
    getVersion(): number {
        return 1;
    }

    getName(): string {
        return "resume";
    }

    getDescription(): string {
        return "Resumes the player";
    }

    getUsage(): string {
        return "";
    }

    getAliases(): string[] {
        return ["unpause"];
    }

    getRoles(): string[] {
        return ["DJ"];
    }

    getCommandOptions(): ApplicationCommandOptionData[] {
        return [];
    }

    getEphemeral(): EphemeralMessageState {
        return EphemeralMessageState.Never;
    }

    async run(bot: MusicBot, interaction: CommandInteraction): Promise<void> {
        if (!bot.channelCheck(interaction)) return;

        const player = bot.playerManager?.get(interaction.guild?.id || "0");
        if (!player) {
            return bot.sendInteractionError(interaction, "Something went wrong retrieving the player");
        }

        if (!player.playing && !player.paused) {
            return bot.sendInteractionError(interaction, "I am currently not playing anything and you want me to resume doing what?");
        }
        if (player.playing && !player.paused) {
            return bot.sendInteractionError(interaction, "I should be playing still, If you dont hear anything, you might have queued a song that is just silence");
        }

        player.pause(false);

        const emb = new MessageEmbed()
            .setColor("GREEN")
            .setAuthor("Player Resumed", bot.config.bot.iconPlay)
            .setDescription("I hope you like whats queued for you");
        return bot.sendInteraction(interaction, this.getEphemeral(), {embeds: [emb]});
    }
}
