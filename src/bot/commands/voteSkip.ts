import {Command} from "../libs/Command";
import {
    ApplicationCommandOptionData, BaseGuildVoiceChannel,
    CommandInteraction,
    GuildMember,
    Message,
    MessageEmbed,
} from "discord.js";
import {EphemeralMessageState} from "../libs/enums";
import {MusicBot} from "../MusicBot";

export class VoteSkip implements Command {
    getVersion(): number {
        return 1;
    }

    getName(): string {
        return "VoteSkip"
    }

    getDescription(): string {
        return "Vote skip a song"
    }

    getUsage(): string {
        return ""
    }

    getAliases(): string[] {
        return []
    }

    getRoles(): string[] {
        return ["EVERYONE"]
    }

    getCommandOptions(): ApplicationCommandOptionData[] {
        return [];
    }

    getEphemeral(): EphemeralMessageState {
        return EphemeralMessageState.Always
    }

    // TODO: implement buttons to interact
    async run(bot: MusicBot, interaction: CommandInteraction): Promise<void> {
        if (!bot.channelCheck(interaction)) return;

        const player = bot.playerManager?.get(interaction.guild?.id || "0");
        if (!player) {
            return bot.sendInteractionError(interaction, "Something went wrong retrieving the player");
        }

        if (!player.playing && !player.paused) {
            return bot.sendInteractionError(interaction, "Skipping over nothing, sure thing...");
        }

        // get current skips
        const skips = player.get<Array<string>>("skips") || [];

        if (skips.indexOf(interaction.user.id) !== -1) {
            return bot.sendInteractionError(interaction, "You have already voted to skip!");
        }

        // increment by one
        skips.push(interaction.user.id);
        player.set("skips", skips);

        // get channel for user list
        const channel = bot.client.channels.cache.get(player.voiceChannel as string);
        if (channel?.isVoice()) {
            // get current users in VC
            const users = this.countUsersInVoice(channel);
            // get needed percentage
            const neededP = bot.config.guildDB.getGuild(interaction.guildId || "").voteSkipPercent || 1;
            // calculate needed skips
            const needed = Math.ceil(users * neededP);

            // remove old voteskip state message
            const ovsm = player.get<Message>("voteSkipMessage");
            if (ovsm && !ovsm.deleted && ovsm.deletable) await ovsm.delete();
            player.set("voteSkipMessage", undefined);

            if (skips.length >= needed) {
                player.stop()

                let msg = "Why was this even queued in the first place?";
                if (player.queue.length) msg += ", off to the next then..";
                const emb = new MessageEmbed()
                    .setColor("PURPLE")
                    .setAuthor("Skipping Song", bot.config.bot.iconNext)
                    .setDescription(msg);
                return await bot.sendInteraction(interaction, EphemeralMessageState.Never, {embeds: [emb]});
            } else {
                // create new state message
                const textChan = bot.client.channels.cache.get(player.textChannel || "");
                if (textChan?.isText()) {
                    const vse = new MessageEmbed()
                        .setColor("ORANGE")
                        .setAuthor("Vote Skip", bot.config.bot.iconVoteSkip)
                        .setDescription("Someone started a Vote Skip for this song.  \nUse `/voteskip` or the actions bellow to cast your vote")
                        .addField("Votes",  skips.length.toString(10) + " / " + needed.toString(10), true);
                    const vsm = await textChan.send({embeds: [vse]});
                    player.set("voteSkipMessage", vsm);
                }

                const emb = new MessageEmbed()
                    .setColor("PURPLE")
                    .setAuthor("Vote Skip", bot.config.bot.iconVoteSkip)
                    .setDescription("Your vote has been registered");
                return await bot.sendInteraction(interaction, this.getEphemeral(), {embeds: [emb]});
            }
        }
    }

    private countUsersInVoice(channel: BaseGuildVoiceChannel): number {
        const members = channel.members.filter((member: GuildMember) => {
            // filter bots
            if (member.user.bot) return false;
            // filter muted
            if (member.voice.deaf) return false;
            return true;
        });
        return members.size;
    }
}
