import {Command} from "../libs/Command";
import {ApplicationCommandOptionData, CommandInteraction} from "discord.js";
import {CommandBuilder} from "../utils/commandBuilder";
import {EphemeralMessageState} from "../libs/enums";
import {MusicBot} from "../MusicBot";

export class Stop implements Command {
    getVersion(): number {
        return 1;
    }

    getName(): string {
        return "stop";
    }

    getDescription(): string {
        return "Stop the player and optionally disconnect the bot";
    }

    getUsage(): string {
        return "";
    }

    getAliases(): string[] {
        return [];
    }

    getRoles(): string[] {
        return ["DJ"];
    }

    getCommandOptions(): ApplicationCommandOptionData[] {
        const options = new CommandBuilder()
            .addBoolInput("disconnect")
            .setDescription("Also disconnect the bot from the Voice Channel (default false)");
        return options.output();
    }

    getEphemeral(): EphemeralMessageState {
        return EphemeralMessageState.Default;
    }

    async run(bot: MusicBot, interaction: CommandInteraction): Promise<void> {
        if (!bot.channelCheck(interaction)) return;

        const player = bot.playerManager?.get(interaction.guild?.id || "0");
        if (!player) {
            return bot.sendInteractionError(interaction, "Something went wrong retrieving the player");
        }

        // command option retrieval
        const disconnect = interaction.options.get("disconnect")?.value || false;

        if (!player.playing && !player.paused) {
            if (!disconnect) {
                return bot.sendInteractionError(interaction, "I am not doing anything, If you want me to disconnect, just say it");
            }
        }

        // stop and clear playlist
        player.queue.clear();
        player.stop()

        // message
        if (disconnect) {
            // delayed disconnect and player destroy so the event system can propagate the music stop events
            setTimeout(() => {
                player.disconnect();
                player.destroy();
                return bot.sendInteraction(interaction, this.getEphemeral(), "Okay, okay. I'll be gone already..");
            }, 500);
        } else {
            return bot.sendInteraction(interaction, this.getEphemeral(), "I'll be quite, I promise!");
        }
    }
}
