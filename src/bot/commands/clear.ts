import {Command} from "../libs/Command";
import {ApplicationCommandOptionData, CommandInteraction, MessageEmbed} from "discord.js";
import {EphemeralMessageState} from "../libs/enums";
import {MusicBot} from "../MusicBot";

export class Clear implements Command {
    getVersion(): number {
        return 1;
    }

    getName(): string {
        return "clear";
    }

    getDescription(): string {
        return "Clears the queue"
    }

    getUsage(): string {
        return "";
    }

    getAliases(): string[] {
        return [];
    }

    getRoles(): string[] {
        return ["DJ"];
    }

    getCommandOptions(): ApplicationCommandOptionData[] {
        return [];
    }

    getEphemeral(): EphemeralMessageState {
        return EphemeralMessageState.Never;
    }

    async run(bot: MusicBot, interaction: CommandInteraction): Promise<void> {
        if (!bot.channelCheck(interaction)) return;

        const player = bot.playerManager?.get(interaction.guild?.id || "0");
        if (!player) {
            return bot.sendInteractionError(interaction, "Something went wrong retrieving the player");
        }

        if (!player.playing) {
            return bot.sendInteractionError(interaction, "I am currently not playing anything and you want me to clear the already empty queue?");
        }

        if (player.queue.length === 0) {
            return bot.sendInteractionError(interaction, "The queue is already empty!");
        }

        player.queue.clear();

        const emb = new MessageEmbed()
            .setColor("RED")
            .setAuthor("Queue Cleared", bot.config.bot.iconPlaylist)
            .setDescription("All those queued songs, just going to waste, gone.. Forever.");
        return bot.sendInteraction(interaction, this.getEphemeral(), {embeds: [emb]});
    }
}
