import {Command} from "../libs/Command";
import {ApplicationCommandOptionData, CommandInteraction, MessageEmbed} from "discord.js";
import {EphemeralMessageState} from "../libs/enums";
import {MusicBot} from "../MusicBot";

export class Shuffle implements Command {
    getVersion(): number {
        return 1;
    }

    getName(): string {
        return "shuffle";
    }

    getDescription(): string {
        return "Shuffle the queue"
    }

    getUsage(): string {
        return "";
    }

    getAliases(): string[] {
        return [];
    }

    getRoles(): string[] {
        return ["DJ"];
    }

    getCommandOptions(): ApplicationCommandOptionData[] {
        return [];
    }

    getEphemeral(): EphemeralMessageState {
        return EphemeralMessageState.Never;
    }

    async run(bot: MusicBot, interaction: CommandInteraction): Promise<void> {
        if (!bot.channelCheck(interaction)) return;

        const player = bot.playerManager?.get(interaction.guild?.id || "0");
        if (!player) {
            return bot.sendInteractionError(interaction, "Something went wrong retrieving the player");
        }

        if (!player.playing || player.queue.length == 0) {
            return bot.sendInteractionError(interaction, "I can't do the shuffle without songs");
        }
        if (player.queue.length == 1) {
            return bot.sendInteractionError(interaction, "Shuffling with 1 song, that seems productive...");
        }

        player.queue.shuffle();

        const emb = new MessageEmbed()
            .setColor("YELLOW")
            .setAuthor("Playlist shuffled", bot.config.bot.iconPlaylist)
            .setDescription("Do the shuffle!");
        return bot.sendInteraction(interaction, this.getEphemeral(), {embeds: [emb]});
    }
}
