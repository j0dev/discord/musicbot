import {Command} from "../libs/Command";
import {ApplicationCommandOptionData, CommandInteraction, MessageEmbed} from "discord.js";
import {CommandBuilder} from "../utils/commandBuilder";
import {EphemeralMessageState} from "../libs/enums";
import {MusicBot} from "../MusicBot";

export class Seek implements Command {
    getVersion(): number {
        return 1;
    }

    getName(): string {
        return "seek";
    }

    getDescription(): string {
        return "Seeks to a position in the song";
    }

    getUsage(): string {
        return "";
    }

    getAliases(): string[] {
        return [];
    }

    getRoles(): string[] {
        return ["DJ"];
    }

    getCommandOptions(): ApplicationCommandOptionData[] {
        const options = new CommandBuilder()
            .addIntInput("seconds")
            .setDescription("Amount of seconds to skip")
            .setRequired(true)
            .addBoolInput("relative")
            .setDescription("Seek relative to the current position? (default false)")
        return options.output();
    }

    getEphemeral(): EphemeralMessageState {
        return EphemeralMessageState.Never;
    }

    async run(bot: MusicBot, interaction: CommandInteraction): Promise<void> {
        if (!bot.channelCheck(interaction)) return;

        const player = bot.playerManager?.get(interaction.guild?.id || "0");
        if (!player) {
            return bot.sendInteractionError(interaction, "Something went wrong retrieving the player");
        }

        if (!player.playing && !player.paused) {
            return bot.sendInteractionError(interaction, "You hear some beats you can skip?... I dont!");
        }

        let amount = interaction.options.getInteger("seconds") as number;
        const relative = interaction.options.getBoolean("relative") as boolean;

        if (relative) {
            const curTime = player.position / 1000;
            amount += curTime;
        }

        player.seek(amount * 1000);

        const emb = new MessageEmbed()
            .setColor("AQUA")
            .setAuthor("Seeking song", bot.config.bot.iconNext)
            .setDescription("Skipping a little forward in the song");
        return bot.sendInteraction(interaction, this.getEphemeral(), {embeds: [emb]});
    }
}
