import {Command} from "../libs/Command";
import {ApplicationCommandOptionData, CommandInteraction, MessageEmbed} from "discord.js";
import {EphemeralMessageState} from "../libs/enums";
import {MusicBot} from "../MusicBot";

export class Pause implements Command {
    getVersion(): number {
        return 1;
    }

    getName(): string {
        return "pause";
    }

    getDescription(): string {
        return "Pause the player";
    }

    getUsage(): string {
        return "";
    }

    getAliases(): string[] {
        return [];
    }

    getRoles(): string[] {
        return ["DJ"];
    }

    getCommandOptions(): ApplicationCommandOptionData[] {
        return [];
    }

    getEphemeral(): EphemeralMessageState {
        return EphemeralMessageState.Never;
    }

    async run(bot: MusicBot, interaction: CommandInteraction): Promise<void> {
        if (!bot.channelCheck(interaction)) return;

        const player = bot.playerManager?.get(interaction.guild?.id || "0");
        if (!player) {
            return bot.sendInteractionError(interaction, "Something went wrong retrieving the player");
        }

        if (!player.playing && !player.paused) {
            return bot.sendInteractionError(interaction, "I am currently not playing anything and you want me to pause doing what?");
        }
        if (!player.playing && player.paused) {
            return bot.sendInteractionError(interaction, "I am already paused, If you would like to unpause me, please use `/resume`");
        }

        player.pause(true);

        const emb = new MessageEmbed()
            .setColor("YELLOW")
            .setAuthor("Player Paused", bot.config.bot.iconPause)
            .setDescription("If you wish to continue listening, you may call upon me once again using `/unpause`");
        return bot.sendInteraction(interaction, this.getEphemeral(), {embeds: [emb]});
    }
}
