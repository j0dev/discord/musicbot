import {Command} from "../libs/Command";
import {Ping} from "./ping";
import {Play} from "./play";
import {Pause} from "./pause";
import {Resume} from "./resume";
import {Skip} from "./skip";
import {VoteSkip} from "./voteSkip";
import {Clear} from "./clear";
import {Stop} from "./stop";
import {Seek} from "./seek";
import {Shuffle} from "./shuffle";

export const commandList: Command[] = [
    new Ping,
    new Play,
    new Pause,
    new Resume,
    new Skip,
    new VoteSkip,
    new Clear,
    new Stop,
    new Seek,
    new Shuffle,
];
