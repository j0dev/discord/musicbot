import {Command} from '../libs/Command';
import {MusicBot} from "../MusicBot";
import {ApplicationCommandOptionData, CommandInteraction, MessageEmbed} from "discord.js";
import {CommandBuilder} from "../utils/commandBuilder";
import {SearchResult} from "erela.js/structures/Manager";
import {EphemeralMessageState} from "../libs/enums";
import {humanDuration} from "../utils/humanDuration";

export class Play implements Command {
    getVersion(): number {
        return 1;
    }

    getName(): string {
        return "play";
    }

    getDescription(): string {
        return "Play a song";
    }

    getUsage(): string {
        return "[song title | url]";
    }

    getAliases(): string[] {
        return [];
    }

    getRoles(): string[] {
        return ["EVERYONE", "DJ"];
    }

    getCommandOptions(): ApplicationCommandOptionData[] {
        const options = new CommandBuilder()
            .addStringInput("song")
              .setDescription("Song name or URL to play")
              .setRequired(true);
        return options.output();
    }

    getEphemeral(): EphemeralMessageState {
        return EphemeralMessageState.Never;
    }

    async run(bot: MusicBot, interaction: CommandInteraction): Promise<void> {
        const member = interaction.guild?.members.cache.get(interaction.member?.user.id || "0");

        if (!bot.channelCheck(interaction, true)) return;

        const player = bot.playerManager?.create({
            guild: interaction.guild?.id || "",
            voiceChannel: member?.voice?.channel?.id || "0",
            textChannel: interaction.channel?.id || "0",
            selfDeafen: false,
        });
        if (!player) return;

        let result: SearchResult | undefined;
        try {
            await bot.sendInteraction(interaction, EphemeralMessageState.Always, "Searching... hold on..");
            result = await player?.search(interaction.options.get("song", true).value as string, member?.user);
        } catch (e) {
            return;
        }
        if (!result) return; // should never happen

        // race condition catching between player destroy and searching
        if (!player) {
            bot.sendInteractionError(interaction, "Yay, you found a bug, you are one of the few to get this silly message, just go and annoy the create and tell him to fix his shit! He will know what to do when you show him this message");
            return;
        }
        switch (result.loadType) {
            case "LOAD_FAILED":
                bot.sendInteractionError(interaction, "Hmm, Something went wrong trying to search for that, would you mind trying that again?");
                return;

            case "NO_MATCHES":
                bot.sendInteractionError(interaction, "I'm sorry, I could not find anything");
                return;

            case "PLAYLIST_LOADED":
                // bot.logger.debug(result);
                bot.sendInteractionError(interaction, "I'm sorry, I currently do not yet support playlists");
                break;

            case "SEARCH_RESULT":
            case "TRACK_LOADED":
                const track = result.tracks[0];
                if (player?.state != "CONNECTED") await player?.connect();
                player.queue.add(track);
                if (!player.playing && !player.paused && !player.queue.length) await player.play();
                // interaction.deleteReply();
                const emb = new MessageEmbed()
                    .setColor("BLUE")
                    .setAuthor("Added to queue", bot.config.bot.iconPlaylist)
                    .setThumbnail(track.displayThumbnail())
                    .setDescription(`[${track.title}](${track.uri})`)
                    .addField("Author", track.author, true);
                if (track.isStream) {
                    emb.addField("Duration", "live", true);
                } else {
                    const duration = humanDuration(track.duration);
                    emb.addField("Duration", `${duration}`, true);
                }
                emb.addField("Requester", `${track.requester}`, true);
                if (player.queue.length) {
                    emb.addField("Queue position", `${player.queue.length}`, true);
                }
                bot.sendInteraction(interaction, this.getEphemeral(), {embeds: [emb], content: null});
                break;
        }
    }
}
