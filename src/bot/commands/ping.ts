import {Command} from "../libs/Command";
import {MusicBot} from "../MusicBot";
import {ApplicationCommandOptionData, CommandInteraction} from "discord.js";
import {CommandBuilder} from "../utils/commandBuilder";
import {EphemeralMessageState} from "../libs/enums";

export class Ping implements Command {
    getVersion(): number {
        return 1;
    }

    getName(): string {
        return "Ping";
    }

    getDescription(): string {
        return "Pings the bot to see if it is still alive";
    }

    getUsage(): string {
        return "";
    }

    getAliases(): string[] {
        return [];
    }

    getRoles(): string[] {
        return ["EVERYONE"];
    }

    getCommandOptions(): ApplicationCommandOptionData[] {
        const options = new CommandBuilder();
        return options.output();
    }

    getEphemeral(): EphemeralMessageState {
        return EphemeralMessageState.Always;
    }

    run(bot: MusicBot, interaction: CommandInteraction): void {
        bot.sendInteraction(interaction, this.getEphemeral(), "pong!");
    }
}
