export const fullChar = "▰";
export const emptyChar = "▱";
export function progressBar(current: number, max: number, size: number): string {
    const percentage = current / max;
    const fullCount =  Math.round(percentage * size);
    const emptyCount = Math.round(Math.abs(percentage -1) * size);
    let bar =  fullChar.repeat(fullCount);
        bar += emptyChar.repeat(emptyCount);
    return bar;
}
