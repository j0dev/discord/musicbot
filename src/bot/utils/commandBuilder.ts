import {ApplicationCommandOptionData} from "discord.js";

export class CommandBuilder {
    options: ApplicationCommandOptionData[] = [];
    index = -1;

    //** new option functions **//
    addStringInput(name?: string): this {
        this.index++;

        const option: ApplicationCommandOptionData = {
            type: "STRING",
            name: name || "",
            description: "",
            required: false,
            choices: [],
        }
        this.options.push(option);
        return this;
    }
    addIntInput(name?: string): this {
        this.index++;

        const option: ApplicationCommandOptionData = {
            type: "INTEGER",
            name: name || "",
            description: "",
            required: false,
            choices: [],
        }
        this.options.push(option);
        return this;
    }
    addBoolInput(name?: string): this {
        this.index++;

        const option: ApplicationCommandOptionData = {
            type: "BOOLEAN",
            name: name || "",
            description: "",
            required: false,
        }
        this.options.push(option);
        return this;
    }

    //** option edit functions **//
    setName(name: string): this {
        this.options[this.index].name = name;
        return this;
    }
    setDescription(description: string): this {
        this.options[this.index].description = description;
        return this;
    }
    setRequired(required: boolean): this {
        const option = this.options[this.index];
        if (option.type === "STRING" || option.type === "INTEGER" || option.type === "BOOLEAN") {
            option.required = required;
        }
        return this;
    }
    // TODO choices

    //** export function **//
    output(): ApplicationCommandOptionData[] {
        return this.options;
    }
}
