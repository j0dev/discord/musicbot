export function humanDuration(duration: number): string {
    duration = duration / 1000;
    const hours = Math.floor(duration / 3600);
    const minutes = Math.floor(duration / 60) - (hours * 3600);
    const seconds = duration - ((hours * 3600) + (minutes * 60))
    let dur = '';
    if (hours > 0) dur += `${hours}:`;
    if (hours > 0 || minutes > 0) dur += `${minutes}:`;
    dur += `${seconds}`;
    return dur;
}
