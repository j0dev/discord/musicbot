import {MusicBot} from "../MusicBot";
import {Guild} from "discord.js";
import {Command} from "../libs/Command";

/**
 * Function to register / update commands
 *
 * @todo implement roles / permissions
 * @todo implement edit / update command
 * @todo figure out deleting of not registered (in the bot) commands
 * @todo figure out error reporting?
 *
 * @param bot
 * @param guild
 */
export function registerCommands(bot: MusicBot, guild: Guild): boolean {
    bot.logger.info("Updating commands for guild: "+ guild.name, {guild_id: guild.id});
    // register slash commands
    const cv = bot.config.guildDB.getGuild(guild.id).state.commandVersion;
    const pl: Promise<void>[] = [];

    bot.commands.forEach((command: Command) => {
        // check for missing command
        if (!cv.has(command.getName())) {
            bot.logger.debug("Registering command: "+ command.getName(), {guild_id: guild.id, command: command.getName()});
            const p = guild.commands.create({
                name: command.getName().toLowerCase(),
                description: command.getDescription(),
                type: "CHAT_INPUT",
                options: command.getCommandOptions(),
                defaultPermission: true,
            }).then(() => {
                cv.set(command.getName(), command.getVersion());
            }).catch(reason => {
                bot.logger.error("Could not create command " + command.getName() + ": " + reason, {guild_id: guild.id, command: command.getName(), error: reason});
            });
            pl.push(p);

        // check for command that needs an update
        } else if ((cv.get(command.getName()) || 0) < command.getVersion()) {
            bot.logger.debug("Updating command: "+ command.getName(), {guild_id: guild.id, command: command.getName(), currentVerison: cv.get(command.getName())});
            //TODO edit command
        }
    });

    // wait for all stuff to complete and save config
    const result = Promise.resolve(Promise.all(pl));
    result.then(value => {
        bot.logger.info("Done updating commands for guild: "+ guild.name, {guild_id: guild.id});
        bot.config.save();
    });

    // TODO success reporting
    return false;
}
