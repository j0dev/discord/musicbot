import {
    Client, CommandInteraction,
    GuildMember,
    Intents, InteractionReplyOptions,
    Message, MessageEmbed, MessageOptions, MessagePayload,
    Snowflake,
    TextBasedChannels
} from "discord.js";
import {Config} from "../config/config";
import {Command} from "./libs/Command";
import {commandList} from "./commands";
import EventEmitter from "events";
import {Event} from "./libs/event";
import {eventList} from "./events";
import {EphemeralMessageState} from "./libs/enums";
import {Manager, NodeOptions} from "erela.js";
import {Logger} from "winston";

export class MusicBot {
    config: Config;
    logger: Logger;
    client: Client;
    commands: Command[] = [];
    events: Event[] = [];
    playerManager: Manager | undefined;

    eventBus = new EventEmitter();

    constructor(config: Config, logger: Logger) {
        this.config = config;
        this.logger = logger;
        this.client = new Client({intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_VOICE_STATES]});
        this.setupLavalink();
    }

    setupLavalink(): void {
        // needed for some callbacks
        const client = this.client;
        // setup nodes
        const nodes: NodeOptions[] = [];
        for (const node of this.config.lavalink.nodes) {
            nodes.push({
                identifier: node.identifier,
                host: node.host,
                port: node.port,
                password: node.password,
                secure: node.secure,
                requestTimeout: this.config.lavalink.requestTimeout,
                retryAmount: this.config.lavalink.retryAmount,
                retryDelay: this.config.lavalink.retryDelay,
            });
        }
        // setup player manager
        this.playerManager = new Manager({
            nodes: nodes,
            send(id, payload) {
                const guild = client.guilds.cache.get(id);
                if(guild) guild.shard.send(payload);
            },
        });

        // lavalink info
        this.playerManager.on("nodeConnect", (node) => {
            this.logger.info("Lavalink node " + node.options.identifier + " Connected", {node: node.options.identifier});
        });
        this.playerManager.on("nodeError", (node, error) => {
            this.logger.error("Lavalink node " + node.options.identifier + " gave an error:", {node: node.options.identifier, error});
        });
        this.playerManager.on("nodeDisconnect", (node, error) => {
            this.logger.error("Lavalink node " + node.options.identifier + " disconnected error:", {node: node.options.identifier, error});
        });
        // fix trackEnd call on last song ending
        this.playerManager.on("queueEnd", async player => {
            this.playerManager?.emit("trackEnd", player);
        });
        // required voice update passthrough
        this.client.on("raw", d => this.playerManager?.updateVoiceState(d));
        // on ready, init the playermanager
        this.client.once("ready", () => {
            this.playerManager?.init(this.client.user?.id);
        });
    }

    run(): void {
        this.commands = commandList;
        this.events = eventList;

        this.loadEvents();

        this.client.login(this.config.bot.token);
    }

    on(eventName: string, listener: (bot: MusicBot, ...args: any[]) => void): this {
        this.eventBus.on(eventName, listener.bind(null, this));
        return this;
    }
    once(eventName: string, listener: (bot: MusicBot, ...args: any[]) => void): this {
        this.eventBus.once(eventName, listener.bind(null, this));
        return this;
    }
    emit(eventName: string, ...args: any[]): boolean {
        return this.eventBus.emit(eventName, args);
    }

    private loadEvents() {
        this.logger.info("Loading events..");
        this.events.forEach((event) => {
            this.logger.debug("Loading "+ event.getType() +" event: "+ event.getEvent(), {event: event.getEvent(), type: event.getType()});
            switch (event.getType()) {
                case "discord":
                    this.client.on(event.getEvent(), event.run.bind(null, this));
                    break;
                case "musicbot":
                    this.on(event.getEvent(), event.run);
                    break;
                case "lavalink":
                case "player":
                    // ignore type errors, there is no way to check properly without doing silly double stuff
                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore
                    this.playerManager?.on(event.getEvent(), event.run.bind(null, this));
                    break;
            }
        });
        this.logger.info("Events loaded");
    }

    /**
     * Send a reply to an interaction
     * @param interaction
     * @param ephemeral
     * @param message
     */
    async sendInteraction(interaction: CommandInteraction, ephemeral: EphemeralMessageState, message: string | InteractionReplyOptions): Promise<any> {
        if (typeof message === "string") {
            message = {content: message};
        }

        const guildDb = this.config.guildDB.getGuild(interaction.guild?.id || "0");

        switch (ephemeral) {
            case EphemeralMessageState.Default:
                message.ephemeral = guildDb.ephemeralReplies;
                break;
            case EphemeralMessageState.Never:
                message.ephemeral = false;
                break;
            case EphemeralMessageState.Always:
                message.ephemeral = true;
        }

        let s;
        if (interaction.replied || interaction.deferred) {
            if (interaction.ephemeral && message.ephemeral === false) {
                s = this.sendMessage(interaction.channelId, message);
            } else {
                s = interaction.editReply(message);
            }
        } else {
            s = interaction.reply(message);
        }
        s.then(() => {
            if (!interaction.ephemeral && guildDb.cleanupTimer) {
                setTimeout(async () => {
                    await interaction.deleteReply();
                }, guildDb.cleanupTimer * 1000);
            }
        });
        return await s;
    }
    /**
     * Send a message to channel
     * @param channel
     * @param message
     */
    async sendMessage(channel: string | TextBasedChannels, message: string | MessagePayload | MessageOptions): Promise<Message | undefined> {
        if (typeof channel === "string") {
            const chan = this.client.channels.cache.get(channel);
            if (!chan || !chan.isText()) return;
            channel = chan;
        }
        if (typeof channel === "string") return; // should never happen
        const msg = channel.send(message);
        msg.then((msg: Message) => {
            const cleanupTimer = this.config.guildDB.getGuild(msg.guild?.id || "0").cleanupTimer;
            if (cleanupTimer) {
                setTimeout(() => {
                    if (!msg.deleted && msg.deletable) msg.delete();
                }, cleanupTimer * 1000);
            }
        });
        return await msg;
    }

    /**
     * Send an error to an interaction
     * @param interaction
     * @param content
     */
    sendInteractionError(interaction: CommandInteraction, content: string | MessageEmbed): Promise<void> {
        if (typeof content !== "object") {
            content = new MessageEmbed()
                .setColor("DARK_RED")
                .setDescription(":x: | "+ content);
        }

        return this.sendInteraction(interaction, EphemeralMessageState.Always, {embeds: [content]});
    }
    /**
     * Send an error to a channel
     * @param channel
     * @param content
     */
    sendError(channel: string | TextBasedChannels, content: string | MessageEmbed): Promise<Message | undefined> {
        if (typeof content !== "object") {
            content = new MessageEmbed()
                .setColor("DARK_RED")
                .setDescription(":x: | "+ content);
        }

        return this.sendMessage(channel, {embeds: [content]});
    }

    /**
     * Helper to get a member from an Id and guild
     * @param id
     * @param guildId
     */
    getMember(id: string | Snowflake, guildId: string | Snowflake): GuildMember | undefined {
        const guild = this.client.guilds.cache.get(guildId);
        if (!guild) return;
        return guild.members.cache.get(id);

    }

    /**
     * helper for interaction checks on the player and their voice presence
     * @param interaction
     * @param partial
     */
    channelCheck(interaction: CommandInteraction, partial?: boolean): boolean {
        const member = this.getMember(interaction.user.id, interaction.guild?.id || "0");
        if (!member) return false;

        if (!partial) {
            const player = this.playerManager?.get(member.guild.id);
            if (!player) {
                this.sendInteractionError(interaction, "I'm sorry, I'm currently not active!");
                return false;
            }
        }

        if (!member.voice.channel) {
            this.sendInteractionError(interaction, "You need to be in A voice channel to use me!");
            return false;
        }

        if (!partial) {
            if (member.voice.channel?.id !== interaction.guild?.me?.voice.channel?.id) {
                this.sendInteractionError(interaction, "You need to be in the same voice channel to control me");
                return false;
            }
        }

        return true;
    }
}
