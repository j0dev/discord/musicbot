import {Event} from "../libs/event";
import {MusicBot} from "../MusicBot";
import {Guild} from "discord.js";

export class GuildDelete implements Event{
    getEvent(): string {
        return "guildDelete";
    }

    getDescription(): string {
        return "Emitted whenever a guild kicks the client or the guild is deleted/left";
    }

    getType(): string {
        return "discord";
    }

    run(bot: MusicBot, guild: Guild): void {
        // remove settings
        bot.config.guildDB.removeGuild(guild.id);
    }
}
