import {Interaction} from "./interaction";
import {Event} from "../libs/event";
import {GuildCreate} from "./guildCreate";
import {GuildDelete} from "./guildDelete";
import {Ready} from "./ready";
import {nowPlayingOnTrackEnd, nowPlayingOnTrackStart} from "./nowPlaying";
import {QueueEnd} from "./queueEnd";
import {VoteSkip} from "./voteSkip";

export const eventList: Event[] = [
    new Ready,
    new Interaction,
    new GuildCreate,
    new GuildDelete,

    new nowPlayingOnTrackStart,
    new nowPlayingOnTrackEnd,

    new QueueEnd,

    new VoteSkip,
];
