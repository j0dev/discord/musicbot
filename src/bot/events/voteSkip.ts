import {Event} from "../libs/event";
import {MusicBot} from "../MusicBot";
import {Player, Track} from "erela.js/structures/Player";
import {Message} from "discord.js";

export class VoteSkip implements Event {
    getEvent(): string {
        return "trackEnd"
    }

    getDescription(): string {
        return "Vote Skip on track end listener to reset state after each song"
    }

    getType(): string {
        return "player"
    }

    // TODO add check if we need to do this
    async run(bot: MusicBot, player: Player, track: Track): Promise<void> {
        // remove old voteskip state message
        const ovsm = player.get<Message>("voteSkipMessage");
        if (ovsm && !ovsm.deleted && ovsm.deletable) await ovsm.delete();
        player.set("voteSkipMessage", undefined);

        // reset skips
        player.set("skips", []);
    }
}
