import {Event} from "../libs/event";
import {MusicBot} from "../MusicBot";
import {Interaction as DInteraction} from "discord.js";

export class Interaction implements Event {
    getEvent(): string {
        return "interactionCreate";
    }

    getDescription(): string {
        return "Event that gets triggered when an interaction is created (e.g. command or button";
    }

    getType(): string {
        return "discord";
    }

    run(bot: MusicBot, interaction: DInteraction): void {
        if (interaction.isCommand()) {
            // TODO build more efficient map?
            for (const command of bot.commands) {
                if (command.getName().toLowerCase() === interaction.commandName || command.getAliases().includes(interaction.commandName)) {
                    bot.logger.debug("Running command "+ command.getName(), {guild_id: interaction.guildId, command: command.getName(), user: interaction.user.id});
                    command.run(bot, interaction);
                    return;
                }
            }
        }
        // TODO handle other interactions
    }
}
