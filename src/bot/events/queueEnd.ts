import {Event} from "../libs/event";
import {MusicBot} from "../MusicBot";
import {Player} from "erela.js/structures/Player";
import {MessageEmbed} from "discord.js";

export class QueueEnd implements Event {
    getEvent(): string {
        return "queueEnd";
    }

    getDescription(): string {
        return "Event called when the queue ends and the last song is done playing";
    }

    getType(): string {
        return "player";
    }

    async run(bot: MusicBot, player: Player): Promise<void> {
        const channel = bot.client.channels.cache.get(player.textChannel || "0");
        if (!channel || !channel.isText()) return // should not happen
        const guildDB = bot.config.guildDB.getGuild(player.guild);

        const emb = new MessageEmbed()
            .setAuthor("Queue Ended", bot.config.bot.iconStop)
            .setDescription("The queue has ended! Anyone got some more for me?")
            .setColor("RANDOM");
        await bot.sendMessage(channel, {embeds: [emb]});

        const dt = guildDB.disconnectTimer;
        if (dt) {
            setTimeout(() => {
                if (!player.playing && !player.paused) {
                    player.disconnect();
                    player.destroy();
                }
            }, dt * 1000);
        }
    }
}
