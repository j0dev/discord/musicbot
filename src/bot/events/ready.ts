import {Event} from "../libs/event";
import {MusicBot} from "../MusicBot";
import {registerCommands} from "../utils/commandRegister";

export class Ready implements Event {
    getEvent(): string {
        return "ready";
    }

    getDescription(): string {
        return "Ready event from discord";
    }

    getType(): string {
        return "discord";
    }

    run(bot: MusicBot): void {
        bot.client.guilds.cache.forEach((guild) => {
            if (!bot.config.guildDB.hasGuild(guild.id)) {
                bot.config.guildDB.createGuild(guild.id);
            }
            registerCommands(bot, guild);
        });
    }
}
