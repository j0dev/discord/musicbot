import {Event} from "../libs/event";
import {MusicBot} from "../MusicBot";
import {Guild} from "discord.js";
import {registerCommands} from "../utils/commandRegister";

export class GuildCreate implements Event{
    getEvent(): string {
        return "guildCreate";
    }

    getDescription(): string {
        return "Emitted whenever the client joins a guild";
    }

    getType(): string {
        return "discord";
    }

    run(bot: MusicBot, guild: Guild): void {
        // create the guild config
        const guidlDB = bot.config.guildDB.createGuild(guild.id);

        // register the commands
        const success = registerCommands(bot, guild);
    }
}
