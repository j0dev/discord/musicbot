import {Event} from "../libs/event";
import {MusicBot} from "../MusicBot";
import {Player, Track} from "erela.js/structures/Player";
import {Message, MessageEmbed} from "discord.js";
import {progressBar} from "../utils/progressBar";
import {humanDuration} from "../utils/humanDuration";

export class nowPlayingOnTrackStart implements Event {
    getEvent(): string {
        return "trackStart";
    }

    getDescription(): string {
        return "Now Playing on track start listener";
    }

    getType(): string {
        return "player";
    }

    async run(bot: MusicBot, player: Player, track: Track): Promise<void> {
        // get channel
        const channel = bot.client.channels.cache.get(player.textChannel || "0");
        if (!channel || !channel.isText()) return // should not happen

        // create message
        const emb = new MessageEmbed();
        emb.setColor("RANDOM");
        // name
        emb.setAuthor("Now Playing", bot.config.bot.iconNowPlaying);
        // thumbnail if available
        if (track.displayThumbnail()) emb.setThumbnail(track.displayThumbnail());
        // track title
        const progress = progressBar(0, track.duration, 20);
        emb.setDescription(`[${track.title}](${track.uri})  \n${progress}`);
        // requester
        emb.addField("Requested by", `${track.requester}`, true);
        // duration
        if (track.isStream) {
            emb.addField("Duration", ":red_circle: live", true);
        } else {
            const duration = humanDuration(track.duration);
            emb.addField("Duration", `\`${duration}\``, true);
        }

        // send
        const nowPlaying = await channel.send({embeds: [emb]});
        // set now playing
        player.set("nowPlayingMessage", nowPlaying);

        // progress update
        const updateTime = (track.duration / 20) / 3;
        const interval = setInterval(async (player: Player) => {
            if (!player) return;
            if (!player.playing && !player.paused) {
                clearInterval(player.get<NodeJS.Timer>("nowPlayingTimer"));
                return;
            }
            const msg = player.get<Message>("nowPlayingMessage");
            const emb = msg.embeds[0];

            const progress = progressBar(player.position, track.duration, 24);
            emb.setDescription(`[${track.title}](${track.uri})  \n${progress}`)

            await msg.edit({embeds: [emb]});

        }, updateTime, player);
        player.set("nowPlayingTimer", interval);
    }
}

export class nowPlayingOnTrackEnd implements Event {
    getEvent(): string {
        return "trackEnd";
    }

    getDescription(): string {
        return "Now Playing on track end listener"
    }

    getType(): string {
        return "player";
    }

    async run(bot: MusicBot, player: Player): Promise<void> {
        // cancel now playing progress bar updates
        const timer = player.get<NodeJS.Timer>("nowPlayingTimer");
        clearInterval(timer);
        // get message
        const nowPlaying: Message = player.get("nowPlayingMessage");
        // remove if still present
        if (nowPlaying && !nowPlaying.deleted && nowPlaying.deletable) await nowPlaying.delete();
    }
}
