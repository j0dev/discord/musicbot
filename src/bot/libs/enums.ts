export enum EphemeralMessageState {
    Never = "NEVER",
    Default = "DEFAULT",
    Always = "ALWAYS"
}
