import {MusicBot} from "../MusicBot";
import {ApplicationCommandOptionData, CommandInteraction} from "discord.js";
import {EphemeralMessageState} from "./enums";

export interface Command {
    /**
     * Version of the command, used for updating discord's command registration
     */
    getVersion(): number;

    /**
     * Name of the command
     */
    getName(): string;

    /**
     * Description of the command
     */
    getDescription(): string;

    /**
     * Usage description of the command
     */
    getUsage(): string;

    /**
     * Aliases of the command
     */
    getAliases(): string[];

    /**
     * Required roles that a user needs to run this
     */
    getRoles(): string[];

    /**
     * Options
     */
    getCommandOptions(): ApplicationCommandOptionData[];

    /**
     * Default response ephemeral type
     */
    getEphemeral(): EphemeralMessageState;

    /**
     * Function that is run every time the command is run
     * @param bot
     * @param interaction
     */
    run(bot: MusicBot, interaction: CommandInteraction): void;
}
