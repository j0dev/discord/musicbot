import {MusicBot} from "../MusicBot";

export interface Event {
    /**
     * Name of the event to listen for
     */
    getEvent(): string;

    /**
     * Description of the event
     */
    getDescription(): string;

    /**
     * Type / Source of the event
     * eg "musicbot" or "discord"
     */
    getType(): string;

    /**
     * Function to run when the event gets triggered
     * @param bot
     * @param args
     */
    run(bot: MusicBot, ...args: any[]): void;
}
