import {Bot} from "./sections/bot";
import {GuildDB} from "./sections/guildDB";
import {Lavalink} from "./sections/lavalink";

/**
 * Configuration fot the whole system
 */
export class Config {
    bot: Bot;
    guildDB: GuildDB;
    lavalink: Lavalink;

    constructor() {
        this.bot = new Bot();
        this.guildDB = new GuildDB();
        this.lavalink = new Lavalink();
    }

    load(): void {
        this.bot.load();
        this.guildDB.load();
        this.lavalink.load();
    }

    save(): void {
        this.bot.save();
        this.guildDB.save();
        this.lavalink.save();
    }
}
