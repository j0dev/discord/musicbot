import {ConfigFile} from "../configFile";
import {ConfigSchema, SchemaFieldString} from "../configSchema";

/**
 * Configuration for the discord bot part
 */
export class Bot extends ConfigFile {
    _filename = 'bot.yaml';

    clientId = '';
    clientSecret = '';
    token = '';

    iconNowPlaying = '';
    iconStop = '';
    iconPlaylist = '';
    iconPlay = '';
    iconPause = '';
    iconNext = '';
    iconVoteSkip = '';

    getSchema(schema: ConfigSchema): ConfigSchema {
        super.getSchema(schema);
        schema.setFilename(this._filename);
        schema.addField(new SchemaFieldString('clientId'));
        schema.addField(new SchemaFieldString('clientSecret'));
        schema.addField(new SchemaFieldString('token'));
        schema.addField(new SchemaFieldString('iconNowPlaying', 'https://j0dev.nl/icons/Device-Headphone-256.png'));
        schema.addField(new SchemaFieldString('iconStop', 'https://j0dev.nl/icons/Power-256.png'));
        schema.addField(new SchemaFieldString('iconPlaylist', 'https://j0dev.nl/icons/Folder-Music-256.png'));
        schema.addField(new SchemaFieldString('iconPlay', 'https://j0dev.nl/icons/Media-Play-256.png'));
        schema.addField(new SchemaFieldString('iconPause', 'https://j0dev.nl/icons/Media-Pause-256.png'));
        schema.addField(new SchemaFieldString('iconNext', 'https://j0dev.nl/icons/Arrowhead-Right-01-256.png'));
        schema.addField(new SchemaFieldString('iconVoteSkip', 'https://j0dev.nl/icons/Clipboard-Next-256.png'));
        return schema;
    }
}
