import {ConfigFile} from "../configFile";
import {ConfigSchema, SchemaField, SchemaFieldSectionArray} from "../configSchema";
import {ConfigSection} from "../section";

/**
 * Configuration for the lavalink / player part
 */
export class Lavalink extends ConfigFile {
    _filename = 'lavalink.yaml';

    requestTimeout = 0;
    retryAmount = 0;
    retryDelay = 0;
    nodes: LavaNode[] = [];


    getSchema(schema: ConfigSchema): ConfigSchema {
        super.getSchema(schema);
        schema.setFilename(this._filename);
        schema.addField(new SchemaField('requestTimeout', 200));
        schema.addField(new SchemaField('retryAmount', 5));
        schema.addField(new SchemaField('retryDelay', 1500));
        schema.addField(new SchemaFieldSectionArray('nodes', LavaNode, true));
        return schema;
    }
}

export class LavaNode extends ConfigSection {
    identifier = '';
    host = '';
    port = 0;
    password = '';
    secure = false;

    getSchema(schema: ConfigSchema): ConfigSchema {
        super.getSchema(schema);
        schema.addField(new SchemaField('identifier', 'Main'));
        schema.addField(new SchemaField('host', '127.0.0.1'));
        schema.addField(new SchemaField('port', 2333));
        schema.addField(new SchemaField('password', 'youshallnotpass'));
        schema.addField(new SchemaField('secure', false));
        return schema;
    }
}
