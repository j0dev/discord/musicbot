import {ConfigSection} from "../section";
import {ConfigSchema, SchemaField, SchemaFieldSection} from "../configSchema";
import {GuildState} from "./guildState";

/**
 * GuildConfig holds the configuration for a specific guild
 */
export class GuildConfig extends ConfigSection {
    state = new GuildState;
    cleanupTimer = 0;
    disconnectTimer = 0;
    ephemeralReplies = true;
    voteSkipPercent = 0.55;

    getSchema(schema: ConfigSchema): ConfigSchema {
        super.getSchema(schema);
        schema.addField(new SchemaFieldSection("state", GuildState));
        schema.addField(new SchemaField("cleanupTimer", 0));
        schema.addField(new SchemaField("disconnectTimer", 60));
        schema.addField(new SchemaField("ephemeralReplies", true));
        schema.addField(new SchemaField("voteSkipPercent", 0.55))
        return schema;
    }
}
