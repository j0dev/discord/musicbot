import {ConfigFile} from "../configFile";
import {GuildConfig} from "./guild";
import {ConfigSchema, SchemaFieldSectionMap} from "../configSchema";

/**
 * GuildDB represents the config file that holds configuration for all guilds
 */
export class GuildDB extends ConfigFile {
    _filename = 'guilds.yaml'

    guilds = new Map<string, GuildConfig>();

    getSchema(schema: ConfigSchema): ConfigSchema {
        super.getSchema(schema);
        schema.addField(new SchemaFieldSectionMap('guilds', GuildConfig));
        return schema;
    }

    /**
     * Check if a guild has configuration
     * @param id
     */
    hasGuild(id: string): boolean {
        return this.guilds.has(id);
    }

    /**
     * Creates the config for a new guild
     * @param id
     */
    createGuild(id: string): GuildConfig {
        // create config
        const gc = new GuildConfig();
        // set defaults
        gc.import({});
        // add to our map
        this.guilds.set(id, gc);
        // return the guild config object
        return gc;
    }

    /**
     * retrieves the config for a guild
     * @param id
     */
    getGuild(id: string): GuildConfig {
        return this.guilds.get(id) as GuildConfig;
    }

    /**
     * removes the config of a guild
     * @param id
     */
    removeGuild(id: string): void {
        this.guilds.delete(id);
    }
}
