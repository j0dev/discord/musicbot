import {ConfigSection} from "../section";
import {ConfigSchema, SchemaFieldMap} from "../configSchema";

export class GuildState extends ConfigSection {
    commandVersion = new Map<string, number>()

    getSchema(schema: ConfigSchema): ConfigSchema {
        super.getSchema(schema);
        schema.addField(new SchemaFieldMap<number>("commandVersion"));
        return schema;
    }
}
