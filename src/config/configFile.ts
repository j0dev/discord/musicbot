import jsyaml from "js-yaml";
import * as fs from "fs";
import {ConfigSection} from "./section";

/**
 * ConfigFile holds the base system for managing configuration files
 * It gives base functionality to load, save to yaml files
 */
export class ConfigFile extends ConfigSection {
    _filename: string;

    constructor(filename?: string) {
        super();
        this._filename = (filename) ? filename : this._schema.getFilename();
    }

    /**
     * Load the configuration class from the set filename
     */
    load(): void {
        // calculate full config file path
        const cfgFile = process.cwd() + '/config/' + this._filename;

        // create empty object for when file does not exist
        let obj: unknown = {};

        try {
            // check if file exists
            fs.accessSync(cfgFile, fs.constants.F_OK | fs.constants.R_OK | fs.constants.W_OK);
            // try and load config file
            const cfg = jsyaml.loadAll(fs.readFileSync(cfgFile, 'utf8'));
            obj = cfg[0]; // use the first document stream
        } catch (e) {
            // file does not exist, nothing to do, it should automatically load with defaults
        }

        // import config
        this.import(obj);
    }

    /**
     * Saves the configuration class to the set filename
     */
    save(): void {
        // calculate full config file path
        const cfgFile = process.cwd() + '/config/' + this._filename;

        // export the config
        const obj = this.export() as any;

        // write out our config
        fs.writeFileSync(cfgFile, jsyaml.dump(obj));
    }
}
