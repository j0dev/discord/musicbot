/**
 * ConfigSchema holds the fields, defaults and optional validation for each options inside of a config (file) definition
 */
import {ConfigSection} from "./section";

export class ConfigSchema {
    protected fields: SchemaField[];
    protected filename: string;

    constructor() {
        this.fields = [];
        this.filename = 'config.yaml'
    }

    /**
     * Adds a field to the scheam
     * @param field
     */
    addField(field: SchemaField): this {
        this.fields.push(field);
        return this;
    }

    /**
     * Get the schema for a given field
     * @param field
     */
    getField(field: string): SchemaField {
        const index = this.fields.findIndex((field1) => {
            return field === field1.getName();
        });
        return this.fields[index];
    }

    /**
     * Set the default filename for the configuration class
     * @param filename
     */
    setFilename(filename: string): this {
        this.filename = filename;
        return this;
    }

    /**
     * Retrieve the filename fo this configuration class
     */
    getFilename(): string {
        return this.filename;
    }

    /**
     * Iterate over the schema fields
     * @param func
     */
    forEach(func: (field: SchemaField) => void): void {
        for (const field of this.fields) {
            func(field);
        }
    }
}

/**
 * SchemaField holds the basics of working with fields inside a schema
 */
export class SchemaField {
    protected fieldName: string;
    protected defaultValue: any;

    constructor(fieldName: string, defaultValue: any) {
        this.fieldName = fieldName;
        this.defaultValue = defaultValue;
    }

    /**
     * Retrieves the field name
     */
    getName(): string {
        return this.fieldName;
    }

    /**
     * Retrieves the default value for this field
     */
    getDefault(): any {
        return this.defaultValue;
    }

    /**
     * Used to map a field from one object to another
     * e.g. for mapping between a read in config and the config class, or vise versa
     * @param src
     * @param dst
     */
    map(src: any, dst: any): void {
        // TODO setup validation
        if (src.hasOwnProperty(this.fieldName)) {
            dst[this.fieldName] = src[this.fieldName];
        } else {
            dst[this.fieldName] = this.defaultValue;
        }
    }

    /**
     * Used to export a configSection to a generic object
     * @param src
     * @param dst
     */
    export(src: ConfigSection, dst: any): void {
        this.map(src, dst);
    }
}

/**
 * String variant
 */
export class SchemaFieldString extends SchemaField {
    protected defaultValue: string;

    constructor(fieldName: string, defaultValue?: string) {
        super(fieldName, defaultValue);
        this.defaultValue = defaultValue || '';
    }

    getDefault(): string {
        return this.defaultValue;
    }
}

/**
 * Map variant
 * should only be used for basic types
 */
export class SchemaFieldMap<s> extends SchemaField {
    constructor(fieldName: string) {
        super(fieldName, "");
    }

    map(src: any, dst: any): void {
        if (src.hasOwnProperty(this.fieldName)) {
            if (typeof src[this.fieldName] === typeof Map) {
                dst[this.fieldName] = src[this.fieldName];
            } else if (typeof src[this.fieldName] === 'object') {
                if (typeof dst[this.fieldName] === 'undefined') {
                    dst[this.fieldName]= new Map<string, s>();
                }

                for (const key in src[this.fieldName]) {
                    dst[this.fieldName].set(key, src[this.fieldName][key]);
                }
            }
        }
    }

    export(src: any, dst: any): void {
        dst[this.fieldName] = {};

        if (src.hasOwnProperty(this.fieldName)) {
            for (const [key, value] of src[this.fieldName]) {
                dst[this.fieldName][key] = value;
            }
        }
    }
}

/**
 * Nested config section variant
 */
export class SchemaFieldSection extends SchemaField {
    sectionType: typeof ConfigSection;

    constructor(sectionName: string, sectionType: typeof ConfigSection) {
        super(sectionName, {});
        this.sectionType = sectionType;
    }

    map(src: any, dst: any): void {
        // instantiate our section
        const section = new this.sectionType();

        if (src.hasOwnProperty(this.fieldName)) {
            // initialize with the given data
            section.import(src[this.fieldName]);
        } else {
            // initialize with defaults
            section.import({});
        }

        // set the section to the destination object
        dst[this.fieldName] = section;
    }

    export(src: any, dst: any): void {
        const cs = src[this.fieldName] as ConfigSection;
        dst[this.fieldName] = cs.export();
    }
}

/**
 * Array section variant
 */
export class SchemaFieldSectionArray extends SchemaFieldSection {
    initOne: boolean;

    constructor(sectionName: string, sectionType: typeof ConfigSection, initOne?: boolean) {
        super(sectionName, sectionType);
        this.initOne = initOne || false;
    }

    map(src: any, dst: any): void {
        if (src.hasOwnProperty(this.fieldName)) {
            if (src[this.fieldName] instanceof Array) {
                dst[this.fieldName] = src[this.fieldName].map((value: any) => {
                    if (typeof value === "object") {
                        const cs = new this.sectionType();
                        cs.import(value);
                        return cs;
                    }
                    return null;
                }).filter((element: any) => {
                    return (element);
                });
            }
        }
        // make it an array if its not yet
        if (!(dst[this.fieldName] instanceof Array)) {
            dst[this.fieldName] = [];
        }
        // initOne?
        if (dst[this.fieldName].length === 0 && this.initOne) {
            const cs = new this.sectionType();
            cs.import({});
            dst[this.fieldName].push(cs);
        }
    }

    export(src: any, dst:any):void {
        dst[this.fieldName] = [];

        if (src.hasOwnProperty(this.fieldName)) {
            for (const value of src[this.fieldName]) {
                const cs = value as ConfigSection;
                dst[this.fieldName].push(cs.export());
            }
        }
    }
}

/**
 * Mapped section variant
 */
export class SchemaFieldSectionMap extends SchemaFieldSection {
    map(src: any, dst: any): void {
        if (src.hasOwnProperty(this.fieldName)) {
            if (typeof src[this.fieldName] === typeof Map) {
                dst[this.fieldName] = src[this.fieldName];
            } else if (typeof src[this.fieldName] === 'object') {
                if (typeof dst[this.fieldName] === 'undefined') {
                    dst[this.fieldName] = new Map<string, ConfigSection>();
                }

                for (const key in src[this.fieldName]) {
                    const cs = new this.sectionType();
                    cs.import(src[this.fieldName][key]);
                    dst[this.fieldName].set(key, cs);
                }
            }
        }
    }

    export(src: any, dst: any): void {
        // init our object
        dst[this.fieldName] = {};

        if (src.hasOwnProperty(this.fieldName)) {
            for (const [key, value] of src[this.fieldName]) {
                const cs = value as ConfigSection;
                dst[this.fieldName][key] = cs.export();
            }
        }
    }
}
