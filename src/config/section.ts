import {ConfigSchema, SchemaField, SchemaFieldString} from "./configSchema";

/**
 * ConfigSection holds the base system for managing configuration objects
 * It gives base functionality to load, save, and do basic operation on configuration
 * This class does however not take care of encoding / decoding of the configuration, only object import and export
 */
export class ConfigSection {
    _schema: ConfigSchema;

    constructor(data?: Object) {
        this._schema = this.getSchema(new ConfigSchema());
        if (data) {
            this.import(data);
        }
    }

    /**
     * getSchema returns the schema definition of the config class
     * @param schema
     */
    getSchema(schema: ConfigSchema): ConfigSchema {
        schema.addField(new SchemaFieldString('version', '1.0.0'));
        return schema;
    }

    /**
     * Load the configuration class from a given data object
     * @param data
     */
    import(data: any): void {
        // map the given data object to our config class
        this._schema.forEach((field: SchemaField) => {
            field.map(data, this);
        });
    }

    /**
     * Saves the configuration class to the set filename
     * @param dst
     */
    export(dst?: Object): Object {
        // create empty config object if no destination is given
        if (!dst) dst = {};

        // map our current config to your object to write out
        this._schema.forEach((field: SchemaField) => {
            field.export(this, dst);
        });

        return dst;
    }

    /**
     * Get the default value for a given field
     * @param field
     */
    getDefault(field: string): any {
        return this._schema.getField(field).getDefault();
    }

    /**
     * Checks if the field currently holds the default value
     * @param field
     */
    isDefault(field: string): boolean {
        const fieldSchema = this._schema.getField(field);
        const obj = this as any;
        return obj[field] === fieldSchema.getDefault();
    }

    /**
     * Reset a field to its default value
     * @param field
     */
    reset(field: string): void {
        const fieldSchema = this._schema.getField(field);
        const obj = this as any;
        return obj[field] = fieldSchema.getDefault();
    }
}
