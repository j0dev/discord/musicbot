import {format} from "winston";
import stringify from "safe-stable-stringify";
import {MESSAGE} from "triple-beam";

export const cli = format((info, opts) => {
    const stringifiedRest = stringify(Object.assign({}, info, {
        level: undefined,
        message: undefined,
        service: undefined,
        section: undefined,
        timestamp: undefined,
    }))

    let message = '';

    if (info.timestamp !== undefined) {
        message += `${info.timestamp} `;
    }
    if (info.service !== undefined) {
        message += `[${info.service}] `;
    }
    if (info.section !== undefined) {
        message += `[${info.section}] `;
    }

    message += `${info.level}: ${info.message}`;

    if (stringifiedRest !== '{}') {
        message += ` ${stringifiedRest}`;
    }

    // set our message
    // @ts-ignore
    info[MESSAGE] = message;

    return info;
});

export const time = format((info, opts) => {
    // if (opts.format) {
    //     info.timestamp = typeof opts.format === 'function'
    //         ? opts.format()
    //         : fecha.format(new Date(), opts.format);
    // }

    if (!info.timestamp) {
        info.timestamp = new Date().toISOString();
    }

    if (opts.alias) {
        info[opts.alias] = info.timestamp;
    }
    return info;
});
