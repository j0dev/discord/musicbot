# ToDo List
## basic stuff
- [ ] Logging configuration (configFile)

## Commands
- [ ] Config command
- [ ] summon command to hail the bot to another channel
- [x] Disconnect / Stop command
- [ ] NowPlaying command
- [x] Pause
- [x] Play
- [ ] (youtube) playlists
- [ ] Search?
- [x] Seek
- [x] Skip
- [x] VoteSkip
- [ ] Queue list
- [ ] Remove command
- [x] Queue clearing
- [x] Shuffle
- [ ] Loop, Song and full queue
- [ ] EQ Tweaks and BassBoost
- [ ] help command?
- [ ] Stats?
- [ ] Volume?

## Mechanics
- [x] NowPlaying messages
- [x] Progress bar for now playing
- [x] Human duration format
- [x] Queue end messages
- [x] leave delayed
- [ ] Pause when everyone leaves
- [ ] Handle admins disconnecting bot unwillingly
